<?php

namespace App\Entity;

class Tokentest
{
    /**
     * @var string
     */
    protected $someText;

    /**
     * @var bool
     */
    protected $acceptTerms;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $save;

    /**
     * @return string
     */
    public function getSomeText(): string
    {
        return $this->someText;
    }

    /**
     * @param string $someText
     */
    public function setSomeText(string $someText): void
    {
        $this->someText = $someText;
    }

    /**
     * @return bool
     */
    public function isAcceptTerms(): bool
    {
        return $this->acceptTerms;
    }

    /**
     * @param bool $acceptTerms
     */
    public function setAcceptTerms(bool $acceptTerms): void
    {
        $this->acceptTerms = $acceptTerms;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     */
    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

    /**
     * @return string
     */
    public function getSave(): string
    {
        return $this->save;
    }

    /**
     * @param string $save
     */
    public function setSave(string $save): void
    {
        $this->save = $save;
    }
}
