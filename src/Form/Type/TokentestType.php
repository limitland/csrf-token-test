<?php

namespace App\Form\Type;

use App\Entity\Tokentest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TokentestType extends AbstractType
{
    public const TOKEN_ID='tokentest';
    public const TOKEN_NAME='_token';

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('someText', TextType::class)
            ->add('acceptTerms', CheckboxType::class)
            ->add('secret', HiddenType::class)
            ->add('submit', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tokentest::class,
            // defaults:
            // 'csrf_protection' => true,
            // 'csrf_field_name' => static::TOKEN_NAME,
            // 'csrf_token_id' => static::TOKEN_ID,
        ]);
    }
}
