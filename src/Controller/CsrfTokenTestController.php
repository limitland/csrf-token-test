<?php

namespace App\Controller;

use App\Entity\Tokentest;
use App\Exception\InvalidCsrfTokenException;
use App\Form\Type\TokentestType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\TokenStorage\SessionTokenStorage;

class CsrfTokenTestController extends AbstractController
{
    /**
     * @param Request $request
     * @param CsrfTokenManagerInterface $tokenManager
     * @param SessionInterface $session
     *
     * @return Response
     *
     * @throws InvalidCsrfTokenException
     */
    public function home(Request $request, CsrfTokenManagerInterface $tokenManager, SessionInterface $session): Response
    {
        $submittedToken = '';

        $entity = new Tokentest();
        $entity->setSomeText('default text');
        $entity->setAcceptTerms(true);

        $form = $this->createForm(TokentestType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $submittedToken = $request->request->get(TokentestType::TOKEN_ID)[TokentestType::TOKEN_NAME] ?? 'invalid';
            if (!$this->isCsrfTokenValid(TokentestType::TOKEN_ID, $submittedToken)) {
                throw new InvalidCsrfTokenException(sprintf(
                    'Invalid CSRF token error: %s',
                    $form->getErrors()->current()->getMessage()
                ));
            }

            // $tokenManager->refreshToken(TokentestType::TOKEN_ID);
        }

        $data = [
            'submitted_token' => $submittedToken,
            'session_token' => $session->get(SessionTokenStorage::SESSION_NAMESPACE . '/' . TokentestType::TOKEN_ID),
        ];

        return $this->renderForm('home/form.html.twig', [
            'form' => $form,
            'data' => $data,
        ]);
    }
}
