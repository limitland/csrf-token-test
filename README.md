# Symfony CSRF token test

This repository contains a test project for the symfony csrf tokens. 

## Installation 

* clone the repository
* run `composer install`
* run `symfony serve`
* browse http://127.0.0.1:8000 
