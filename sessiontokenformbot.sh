#!/bin/sh

TOKEN=$(curl -s --cookie-jar cookies.txt 127.0.0.1:8000 | grep tokentest__token | cut -d'"' -f56 )
for i in {1..10}; do
  curl -s --cookie cookies.txt -X POST -d "tokentest[acceptTerms]=1&tokentest[someText]=${i}&tokentest[_token]=${TOKEN}" 127.0.0.1:8000
done
